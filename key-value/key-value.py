import argparse
import json
import os
from pathlib import Path
import sys


#######################################################################################################################
# Constants
#######################################################################################################################


# If a data file is not provided, this environment variable will be searched for.
DATA_FILE_ENVIRONMENT_VARIABLE = "KEY_VALUE_CLI_FILE"
# If a data file is not provided, and not found in the environment variable,
#   this will be used as the data file.
DEFAULT_DATA_FILE = str(Path.home()) + "/.config/key-value.json"
# These are the exit codes the program may exit with
STATUS_CODES = {"success": 0,
                "no_changes_made": 1,
                "no_data_found": 2,
                "invalid_action": 126,
                "fatal_data_file_error": 127}


#######################################################################################################################
# Global Variables
#######################################################################################################################


# This will be populated from the arguments passed in to the program, as a dictionary.
args = None
# This is where the key-value data is stored as a dictionary.
key_value_data = None


#######################################################################################################################
# Main Processing
#######################################################################################################################


def main():
    """Main processing logic."""
    process_arguments()
    set_data_file()
    check_data_file_exists()
    load_data_file()
    action = args["action"]
    if action == "add":
        action_add(args["key"], args["value"], args["update_on_key_exists"], args["verbose"])
    elif action in ["update", "upd"]:
        action_update(args["key"], args["value"], args["add_on_key_not_exists"], args["verbose"])
    elif action in ["delete", "del"]:
        action_delete(args["key"], args["verbose"])
    elif action == "get":
        action_get(args["key"], args["verbose"])
    elif action == "list":
        action_list(args["delimiter"], args["human_readable"], args["key_substring"], args["value_substring"],
                    args["verbose"])
    else:
        # You should never get here
        error(f"Invalid Action: {action}", STATUS_CODES[""])
    sys.exit(STATUS_CODES["success"])


def process_arguments():
    """Parse / Validate the arguments."""
    global args
    parser = argparse.ArgumentParser(description="Key-Value Utility",
                                     allow_abbrev=False)
    parser.add_argument("-v", "--verbose",
                        help="Display verbose output",
                        dest="verbose",
                        action="store_true")
    parser.add_argument("-f", "--file",
                        help="The key-value JSON data file to use",
                        dest="data_file_name",
                        metavar="file_name")
    parser.add_argument("-c", "--create-file",
                        help="Create key-value data file if it does not exist",
                        dest="create_data_file",
                        action="store_true")
    subparsers = parser.add_subparsers(title="Actions",
                                       dest="action",
                                       metavar="action",
                                       required=True)
    # Add action
    action_add_parser = subparsers.add_parser("add",
                                              description="Add a key-value entry",
                                              help="Add a key-value entry")
    action_add_parser.add_argument("-u", "--update",
                                   action="store_true",
                                   dest="update_on_key_exists",
                                   help="Perform an update if the key already exists.")
    action_add_parser.add_argument("key",
                                   help="The key to add")
    action_add_parser.add_argument("value",
                                   help="The value of the key to add")
    # Update action
    action_update_parser = subparsers.add_parser("update", aliases=["upd"],
                                                 description="Update a key-value entry",
                                                 help="Update a key-value entry")
    action_update_parser.add_argument("-a", "--add",
                                      action="store_true",
                                      dest="add_on_key_not_exists",
                                      help="Perform an add if the key does not already exist")
    action_update_parser.add_argument("key",
                                      help="The key to update")
    action_update_parser.add_argument("value",
                                      help="The value of the key to update")
    # Delete action
    action_delete_parser = subparsers.add_parser("delete", aliases=["del"],
                                                 description="Delete a key-vale entry",
                                                 help="Delete a key-value entry")
    action_delete_parser.add_argument("key",
                                      help="The key to delete")
    # Get action
    action_get_parser = subparsers.add_parser("get",
                                              description="Get the value of the given key",
                                              help="Get the value of the given key")
    action_get_parser.add_argument("key",
                                   help="The key of the value to be retrieved")
    # List action
    action_list_parser = subparsers.add_parser("list",
                                               description="List the key-value entries",
                                               help="List the key-value entries")
    action_list_parser.add_argument("-d", "--delimiter",
                                    default="=",
                                    metavar="delimiter",
                                    dest="delimiter",
                                    help="The character(s) to put between the key and value")
    action_list_parser.add_argument("-H", "--human-readable",
                                    action="store_true",
                                    dest="human_readable",
                                    help="Format the output for better human readability")
    action_list_parser.add_argument("--key-sub",
                                    dest="key_substring",
                                    metavar="substring",
                                    help="Filter entries with keys that have a given substring")
    action_list_parser.add_argument("--value-sub",
                                    dest="value_substring",
                                    metavar="substring",
                                    help="Filter entries with values that have a given substring")
    # Parse it!
    args = vars(parser.parse_args())


#######################################################################################################################
# Actions
#######################################################################################################################


def action_add(key, value, update_on_key_exists, verbose):
    """Add an entry to the key-value list."""
    if key in key_value_data:
        if update_on_key_exists:
            action_update(key, value, False, verbose)
        else:
            error_msg = None
            if verbose:
                error_msg = f"Key already exists: {key}"
            error(error_msg, STATUS_CODES["no_changes_made"])
    else:
        key_value_data[key] = value
        save_data_file()
        if verbose:
            print(f"Added, Key: {key}, Value: {value}")


def action_update(key, value, add_on_key_not_exists, verbose):
    """Update an entry in the key-value list."""
    if key in key_value_data:
        old_value = key_value_data[key]
        if old_value == value:
            error_msg = None
            if verbose:
                error_msg = f"Old Value = New Value, Key: {key}, Value: {value}"
            error(error_msg, STATUS_CODES["no_changes_made"])
        else:
            key_value_data[key] = value
            save_data_file()
            if verbose:
                print(f"Updated, Key: {key}, Old Value: {old_value}, New Value: {value}")
    else:
        if add_on_key_not_exists:
            action_add(key, value, False, verbose)
        else:
            error_msg = None
            if verbose:
                error_msg = f"Key does not exist: {key}"
            error(error_msg, STATUS_CODES["no_changes_made"])


def action_delete(key, verbose):
    """Delete an entry from the key-value list."""
    if key in key_value_data:
        value = key_value_data[key]
        del key_value_data[key]
        save_data_file()
        if verbose:
            print(f"Deleted, Key: {key}, Value: {value}")
    else:
        error_msg = None
        if verbose:
            error_msg = f"Key does not exist: {key}"
        error(error_msg, STATUS_CODES["no_changes_made"])


def action_get(key, verbose):
    """Retrieve / print the value for a given key."""
    if key in key_value_data:
        print(key_value_data[key])
    else:
        error_msg = None
        if verbose:
            error_msg = f"Key does not exist: {key}"
        error(error_msg, STATUS_CODES["no_data_found"])


def action_list(delimiter, human_readable, key_substring, value_substring, verbose):
    """Print the list of key-value entries, applying the supplied filters."""
    # Remove any entries where the key does not have the given substring
    if key_substring:
        working_dict = key_value_data.copy()
        for key in working_dict.keys():
            if key_substring not in key:
                del key_value_data[key]
    # Remove any entries where the value does not have the given substring
    if value_substring:
        working_dict = key_value_data.copy()
        for key in working_dict.keys():
            if value_substring not in key_value_data[key]:
                del key_value_data[key]
    if len(key_value_data) > 0:
        # Get the keys, sort them
        keys_list = list(key_value_data.keys())
        keys_list.sort()
        if not human_readable:
            # Print the list unformatted, best for computer input
            for key in keys_list:
                value = key_value_data[key]
                print(key + delimiter + value)
        else:
            max_key_length = max(len(key) for key in key_value_data.keys())
            key_format = "{:<" + str(max_key_length) + "}"
            for key in keys_list:
                value = key_value_data[key]
                print(key_format.format(key) + delimiter + value)
    else:
        error_msg = None
        if verbose:
            error_msg = "No data found or matched criteria"
        error(error_msg, STATUS_CODES["no_data_found"])


#######################################################################################################################
# Data File
#######################################################################################################################


def set_data_file():
    """Determine the data file to use."""
    if args["data_file_name"] is None:
        # The user did not pass in a file name, see if the environment variable is set
        env_file = os.getenv(DATA_FILE_ENVIRONMENT_VARIABLE)
        if env_file is None:
            # The environment variable is not set, set the default file
            args["data_file_name"] = DEFAULT_DATA_FILE
        else:
            args["data_file_name"] = env_file


def check_data_file_exists():
    """See if the data file exists, create if needed (or error out)."""
    file_path = Path(args["data_file_name"])
    if file_path.exists():
        if not file_path.is_file():
            error("File path is not a file: " + str(file_path), STATUS_CODES["fatal_data_file_error"])
    elif args["create_data_file"]:
        try:
            # Create an empty JSON file
            with open(args["data_file_name"], "w") as f:
                f.write("{}")
        except Exception as e:
            error("Error creating empty JSON data file: " + str(e.args), STATUS_CODES["fatal_data_file_error"])
    else:
        error("File does not exist: " + str(file_path), STATUS_CODES["fatal_data_file_error"])


def load_data_file():
    """Read JSON data file."""
    global key_value_data
    try:
        with open(args["data_file_name"], "r") as f:
            key_value_data = json.load(f)
    except Exception as e:
        error("Error loading key-value data: " + str(e.args), STATUS_CODES["fatal_data_file_error"])


def save_data_file():
    """Save JSON data file."""
    try:
        with open(args["data_file_name"], "w") as f:
            json.dump(key_value_data, f, indent=4, sort_keys=True)
    except Exception as e:
        error("Error saving key-value data: " + str(e.args), STATUS_CODES["fatal_data_file_error"])


#######################################################################################################################
# Logging / Exiting
#######################################################################################################################


def error(text, status_code):
    """Convenience method to print to stderr, and exit with a status_code."""
    if text is not None:
        print(text, file=sys.stderr)
    sys.exit(status_code)


#######################################################################################################################
# Ignition
#######################################################################################################################


if __name__ == "__main__":
    main()
