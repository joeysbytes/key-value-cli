# Key-Value CLI

A command-line utility (written in Python) which manages key / value data and persists it in JSON format.

## Optional Parameters

These are global options for the command.  At a minimum a data file must be provided or systematically found.

| Options | Description |
| --- | --- |
| *action* | add, update, delete, get, list - See below for details |
| -f, --file | The data file to use, in JSON format.  If this is not provided, the environment parameter *KEY_VALUE_CLI_FILE* will be searched for, and if this is not found, the file *.key_value.json* will be searched for in the user's home directory. |
| -c, --create-file | If the data file does not exist, create a new blank one. |
| -v, --verbose | By default this utility only outputs information if it is requested, or on errors.  Turning on verbose will also output status messages. |
| -h, --help | Print help usage information. |

## Add Parameters

**Action:** add

These are the options for adding a key-value entry.

| Options | Description |
| --- | --- |
| key | The key of the entry to add. |
| value | The value of the entry to add. |
| -u, --update | If the entry already exists, update it. |
| -h, --help | Print help usage information. |

## Update Parameters

**Action:** update, upd

These are the options for updating a key-value entry.

| Options | Description |
| --- | --- |
| key | The key of the entry to update. |
| value | The value of the entry to update. |
| -a, --add | If the entry does not exist, add it. |
| -h, --help | Print help usage information. |

## Delete Parameters

**Action:** delete, del

These are the options for deleting a key-value entry.

| Options | Description |
| --- | --- |
| key | The key of the entry to delete. |
| -h, --help | Print help usage information. |

## Get Parameters

**Action:** get

These are the options to get the value of a key-value entry.  Only the value is output, making this action very useful in other scripts.

| Options | Description |
| --- | --- |
| key | The key of the entry to get. |
| -h, --help | Print help usage information. |

## List Parameters

**Action:** list

These are the options to list the values in the key-value dictionary.  This can be useful in scripts as well as  just viewing the data on the screen. 

| Options | Description |
| --- | --- |
| -d, --delimiter | Delimiter to put between the key and value.  Default is an equal sign (=). |
| -H, --human-readable | Format the output for human readability (ie, make the columns fit the data). |
| --key-sub | Filter the keys that have a given substring. |
| --value-sub | Filter the values that have a given substring. |
| -h, --help | Print help usage information. |

## Return Status Codes

Any messages associated with a non-zero return code are written to stderr.

| Status Code | Description |
| --- | --- |
| 0 | Success |
| 1 | A change was requested, but no change occurred. |
| 2 | Data was requested but none was found. |
| 126 | An invalid action was submitted (this should never happen). |
| 127 | A fatal error occurred with the data file. |
